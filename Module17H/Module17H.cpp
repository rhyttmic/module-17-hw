#include <iostream>

using namespace std;

class Vector
{
public:
	Vector()
	{}
	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{}
	void Show()
	{
	  cout << "Модуль вектора = " << x/y/z;
	  cout << '\n' << x << ' ' << y << ' ' << z;
	}
private:
	double x = 10;
	double y = 5;
	double z = 2;
};

int main()
{
	Vector v;
	v.Show();
}
